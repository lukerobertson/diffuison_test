const diffusion = require('diffusion')
const moment = require('moment')
const _ = require('lodash')

let diffusionSession = null

async function main() {
    if (diffusionSession) diffusionSession.close()

    const parseLeafCategory = val => {
        const resultArray = val.toString().split('\u0002')
        const keys = [
            'rp_outcome_uid',
            'bk_outcome_uid',
            'bk_sp_uid',
            'bk_price_uid',
            'bk_event_uid',
            'bk_meeting_uid',
            'status',
            'odds',
            'decimal',
            'hist_f1',
            'hist_f2',
            'hist_f3'
        ]
        return _.zipObject(keys, resultArray)
    }

    diffusion
        .connect({
            // host: 'push-dev.racingpost.com',
            // port: 80
            host: 'push-retail.racingpost.com',
            port: '443',
            secure: 'true'
        })
        .then(
            session => {
                diffusionSession = session

                console.log('Connected to diffusion')

                const todayDate = moment().format('YYYY-MM-DD')

                const subLink = session
                    .subscribe(`?GREYHOUNDS/${todayDate}/.*/.*/OUTRIGHT%20WINNER/.*/#BESTODDS`)
                    .transform(parseLeafCategory)

                // THE BELOW WORKS FOR HORSES FINE
                // const subLink = session
                //     .subscribe(`?HORSES/${todayDate}/.*/.*/WIN/.*/#BESTODDS`)
                //     .transform(parseLeafCategory)

                subLink.on('update', (value, topic) => {
                    console.log(topic, value.odds)
                })
            },
            error => {
                // Failed to connect
                console.log(error, 'will try agian')
                main()
            }
        )
}

if (require.main === module) {
    main().then(() => console.log('fin')).catch(e => console.log(e, 'error, broken'))
}

module.exports = main
